package com.sample.lem.calendar.domain.extensions

import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

fun String.toDate() : Date {
    val localDate = LocalDate.parse(this, DateTimeFormatter.ofPattern("MM-dd-yyyy"))
    return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant())
}