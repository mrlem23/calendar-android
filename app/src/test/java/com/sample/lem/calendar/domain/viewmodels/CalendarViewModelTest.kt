package com.sample.lem.calendar.domain.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.sample.lem.calendar.core.domain.viewmodels.CalendarViewModel
import com.sample.lem.calendar.domain.extensions.toDate
import com.sample.lem.calendar.domain.repository.MockHolidayRepository
import com.sample.lem.calendar.core.data.models.Holiday
import com.sample.lem.calendar.presentation.TestCoroutineDispatcher
import com.sample.lem.calendar.rules.TestCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CalendarViewModelTest {
    private lateinit var calendarViewModel: CalendarViewModel

    @ExperimentalCoroutinesApi
    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        calendarViewModel = CalendarViewModel(MockHolidayRepository(), TestCoroutineDispatcher())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `same date should be zero`() = testCoroutineRule.runBlockingTest {
        val startDate = "09-01-2021".toDate()
        val endDate = "09-01-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        assert(calendarViewModel.numberOfWeekDays.value == 0)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `consecutive days should be zero`() = testCoroutineRule.runBlockingTest {
        val startDate = "09-01-2021".toDate()
        val endDate = "09-02-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        assert(calendarViewModel.numberOfWeekDays.value == 0)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `monday to wed should be 1`() = testCoroutineRule.runBlockingTest {
        val startDate = "08-02-2021".toDate()
        val endDate = "08-04-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        assert(calendarViewModel.numberOfWeekDays.value == 1)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `1 week, monday to monday should be 4`() = testCoroutineRule.runBlockingTest {
        val startDate = "08-02-2021".toDate()
        val endDate = "08-09-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        assert(calendarViewModel.numberOfWeekDays.value == 4)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fixed holiday over the weekend should not affect count`() = testCoroutineRule.runBlockingTest {
        val startDate = "08-30-2021".toDate()
        val endDate = "09-06-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        assert(calendarViewModel.numberOfWeekDays.value == 4)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fixed holiday on a weekday should affect count`() = testCoroutineRule.runBlockingTest {
        val startDate = "09-08-2021".toDate()
        val endDate = "09-14-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        print(calendarViewModel.numberOfWeekDays.value)
        assert(calendarViewModel.numberOfWeekDays.value == 2)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `recurring holiday on a weekday should affect count`() = testCoroutineRule.runBlockingTest {
        val startDate = "09-06-2021".toDate()
        val endDate = "09-10-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        print(calendarViewModel.numberOfWeekDays.value)
        assert(calendarViewModel.numberOfWeekDays.value == 2)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `weekday holiday falling on weekend should affect count`() = testCoroutineRule.runBlockingTest {
        val startDate = "09-13-2021".toDate()
        val endDate = "09-21-2021".toDate()

        // Create observer - no need for it to do anything!
        val numberObserver = Observer<Int> { }
        val holidaysObserver = Observer<Map<String, List<Holiday>>> { }

        calendarViewModel.numberOfWeekDays.observeForever(numberObserver)
        calendarViewModel.holidaysInRange.observeForever(holidaysObserver)

        calendarViewModel.calculateWorkDayDifference(startDate, endDate)
        print(calendarViewModel.numberOfWeekDays.value)
        assert(calendarViewModel.numberOfWeekDays.value == 4)
    }
}