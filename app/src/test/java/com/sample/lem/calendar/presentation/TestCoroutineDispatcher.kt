package com.sample.lem.calendar.presentation

import com.sample.lem.calendar.core.presentation.dispatchers.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.Dispatcher
import kotlin.coroutines.CoroutineContext

class TestCoroutineDispatcher : CoroutineDispatcher {
    override val IO: CoroutineContext = Dispatchers.Unconfined
}