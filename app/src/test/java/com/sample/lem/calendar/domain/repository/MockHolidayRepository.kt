package com.sample.lem.calendar.domain.repository

import com.sample.lem.calendar.core.domain.repository.HolidayRepository
import com.sample.lem.calendar.core.data.models.Holiday
import java.util.*

class MockHolidayRepository : HolidayRepository {
    override suspend fun getHolidays(): List<Holiday> {
        return arrayListOf(
            Holiday.fixed(
                name = "Holiday Fixed weekend",
                month = Calendar.SEPTEMBER,
                date = 5,
            ),
            Holiday.fixed(
                name = "Holiday Fixed 1",
                month = Calendar.SEPTEMBER,
                date = 10,
            ),
            Holiday.fixed(
                name = "Holiday Fixed 2",
                month = Calendar.SEPTEMBER,
                date = 10,
            ),
            Holiday.weekday(
                name = "Holiday Weekday1",
                month = Calendar.SEPTEMBER,
                date = 19,
            ),
            Holiday.recurringDay(
                name = "Holiday Recurring",
                month = Calendar.SEPTEMBER,
                dayOfWeek = Calendar.TUESDAY,
                weekOfMonth = 2
            )
        )
    }
}