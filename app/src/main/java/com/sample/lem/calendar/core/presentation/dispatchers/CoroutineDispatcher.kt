package com.sample.lem.calendar.core.presentation.dispatchers

import kotlin.coroutines.CoroutineContext

// Allows us to easily mock coroutine/suspended
// functions in our unit test
interface CoroutineDispatcher {
    // IO only for now
    val IO : CoroutineContext
}