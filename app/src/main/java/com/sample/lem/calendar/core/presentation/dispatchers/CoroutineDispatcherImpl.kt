package com.sample.lem.calendar.core.presentation.dispatchers

import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class CoroutineDispatcherImpl @Inject constructor(): CoroutineDispatcher {
    override val IO: CoroutineContext = Dispatchers.IO
}