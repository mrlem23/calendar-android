package com.sample.lem.calendar.core.presentation.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.applandeo.materialcalendarview.CalendarView
import com.sample.lem.calendar.R
import com.sample.lem.calendar.core.data.models.Holiday
import com.sample.lem.calendar.core.domain.extensions.generateReadableDate
import com.sample.lem.calendar.core.domain.viewmodels.CalendarViewModel
import com.sample.lem.calendar.core.presentation.adapters.HolidayRecyclerViewAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class CalendarActivity : AppCompatActivity() {
    private val calendarView: CalendarView by lazy { findViewById(R.id.calendarView) }
    private val groupInfo : View by lazy { findViewById(R.id.groupInfo) }
    private val tvStartDate : TextView by lazy { findViewById(R.id.tvStartDate) }
    private val tvEndDate : TextView by lazy { findViewById(R.id.tvEndDate) }
    private val tvDaysInBetween : TextView by lazy { findViewById(R.id.tvDaysInBetween) }
    private val tvHolidaysInBetween : TextView by lazy { findViewById(R.id.tvHolidaysInBetween) }
    private val rvHolidays : RecyclerView by lazy { findViewById(R.id.rvHolidays) }

    private val calendarViewModel by lazy {
        ViewModelProvider(this).get(CalendarViewModel::class.java)
    }

    var currentSelectedDates : ArrayList<Calendar> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeViews()
        observeValues()
    }

    private fun initializeViews() {

        calendarView.setOnDayClickListener { eventDay ->
            if(currentSelectedDates.isEmpty()) {
                currentSelectedDates.add(eventDay.calendar)
            } else if(eventDay.calendar.time != currentSelectedDates[0].time) {
                currentSelectedDates.add(eventDay.calendar)
                currentSelectedDates.sortBy { it.time.time }
                calendarViewModel.calculateWorkDayDifference(currentSelectedDates[0].time, currentSelectedDates[1].time)
            }
        }
    }

    private fun observeValues(){
        calendarViewModel.numberOfWeekDays.observe(this, { totalDays ->
            if(currentSelectedDates.size > 1) {
                groupInfo.visibility = View.VISIBLE
                updateRangeHeader()
                currentSelectedDates.clear()
                updateWorkDays(totalDays)
            }
        })
        calendarViewModel.holidaysInRange.observe(this, { holidayMap ->
            groupInfo.visibility = View.VISIBLE
            updateListOfHolidays(holidayMap)
        })
    }

    private fun updateListOfHolidays(holidayMap : Map<String, List<Holiday>>){
        tvHolidaysInBetween.text = "${holidayMap.values.flatten().size} Holidays"

        rvHolidays.layoutManager = LinearLayoutManager(this)

        rvHolidays.adapter = HolidayRecyclerViewAdapter(holidayMap)
    }

    private fun updateRangeHeader() {
        tvStartDate.text = currentSelectedDates[0].time.generateReadableDate()
        tvEndDate.text = currentSelectedDates[1].time.generateReadableDate()
    }

    @SuppressLint("SetTextI18n")
    private fun updateWorkDays(totalDays : Int){
        tvDaysInBetween.text = "$totalDays Work Days"
    }
}


