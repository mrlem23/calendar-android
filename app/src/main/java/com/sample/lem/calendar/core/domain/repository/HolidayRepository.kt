package com.sample.lem.calendar.core.domain.repository

import com.sample.lem.calendar.core.data.models.Holiday


interface HolidayRepository {
    suspend fun getHolidays() : List<Holiday>
}