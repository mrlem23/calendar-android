package com.sample.lem.calendar.core.presentation.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CalendarApplication : Application() {
}