package com.sample.lem.calendar.core.domain.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.lem.calendar.core.domain.extensions.generateReadableDate
import com.sample.lem.calendar.core.domain.repository.HolidayRepository
import com.sample.lem.calendar.core.presentation.dispatchers.CoroutineDispatcher
import com.sample.lem.calendar.core.data.models.Holiday
import com.sample.lem.calendar.core.data.models.HolidayType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CalendarViewModel @Inject constructor(
    private val holidayRepository: HolidayRepository,
    private val coroutineDispatcher: CoroutineDispatcher
) : ViewModel() {
    val numberOfWeekDays = MutableLiveData(0)
    val holidaysInRange = MutableLiveData<Map<String, List<Holiday>>>()

    private var currentHolidays: List<Holiday>? = null

    fun calculateWorkDayDifference(
        startDate: Date,
        endDate: Date
    ) {
        viewModelScope.launch(coroutineDispatcher.IO) {
            val holidaysInRange = mutableMapOf<String, List<Holiday>>()
            // Cache current holidays
            currentHolidays = currentHolidays ?: holidayRepository.getHolidays()

            // Make map of holidays, key will be used for checking later
            // Done to avoid O(n) traversal of holidays in checking the date
            val holidayMap = mutableMapOf<String, ArrayList<Holiday>>()

            currentHolidays?.forEach { holiday ->
                holidayMap[holiday.key()]?.apply {
                    add(holiday)
                } ?: arrayListOf(holiday).apply {
                    holidayMap[holiday.key()] = this
                }
            }

            val currentDate = Calendar.getInstance()
            currentDate.time = startDate
            // Start one day after start date
            currentDate.add(Calendar.DATE, 1)

            var ctr = 0
            var modifier = 1;

            while (currentDate.time.before(endDate)) {
                val holidaysOnDay = fetchHolidaysOnDay(
                    holidayMap,
                   currentDate
                )

                if (isWeekDay(currentDate)) {
                    if (holidaysOnDay.isEmpty()) {
                        ctr += modifier
                    } else {
                        holidaysInRange[currentDate.time.generateReadableDate()] = holidaysOnDay
                    }
                    modifier = 1
                } else if (hasWeekDayHoliday(holidaysOnDay)) { // Weekday Holidays will always be last on list
                    holidaysInRange[currentDate.time.generateReadableDate()] = holidaysOnDay

                    // Next upcoming weekday will not count
                    modifier = 0
                }
                currentDate.add(Calendar.DATE, 1)
            }

            this@CalendarViewModel.holidaysInRange.postValue(holidaysInRange)
            numberOfWeekDays.postValue(ctr)
        }
    }

    private fun isWeekDay(currentDate: Calendar) : Boolean {
        val dayOfWeek = currentDate.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY
    }

    private fun hasWeekDayHoliday(holidaysOnDay : List<Holiday>) : Boolean{
        return holidaysOnDay.count { it.holidayType == HolidayType.Weekday } > 0
    }


    private fun fetchHolidaysOnDay(
        holidayMap: Map<String, List<Holiday>>,
        currentDate : Calendar
    ): List<Holiday> {
        val holidays = ArrayList<Holiday>();

        val month = currentDate.get(Calendar.MONTH)
        val date = currentDate.get(Calendar.DAY_OF_MONTH)
        val dayOfWeek = currentDate.get(Calendar.DAY_OF_WEEK)
        val weekOfMonth = currentDate.get(Calendar.WEEK_OF_MONTH)

        val keyFixedHoliday = HolidayType.Fixed.generateKey(month = month, date = date)

        holidayMap[keyFixedHoliday]?.apply {
            holidays.addAll(this)
        }

        val keyRecurringHoliday = HolidayType.RecurringDay.generateKey(
            month = month,
            dayOfWeek = dayOfWeek,
            weekOfMonth = weekOfMonth
        )
        holidayMap[keyRecurringHoliday]?.apply {
            holidays.addAll(this)
        }

        // Weekday Holidays will always take precedence since it has an effect on count
        val keyWeekDayHoliday = HolidayType.Weekday.generateKey(month = month, date = date)
        holidayMap[keyWeekDayHoliday]?.apply {
            holidays.addAll(this)
        }

        return holidays
    }
}

