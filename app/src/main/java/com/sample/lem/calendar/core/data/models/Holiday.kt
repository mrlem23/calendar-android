package com.sample.lem.calendar.core.data.models

import javax.annotation.Nullable

class Holiday private constructor(
    val holidayType: HolidayType,
    val name: String,
    val month: Int,
    @Nullable var date: Int = -1,
    @Nullable var dayOfWeek: Int = -1,
    @Nullable var weekOfMonth: Int = -1,
) {

    companion object {
        fun fixed(
            name: String,
            month: Int,
            date: Int = -1,
        ): Holiday {
            return Holiday(holidayType = HolidayType.Fixed, month = month, date = date, name = name)
        }

        fun weekday(
            name: String,
            month: Int,
            date: Int = -1,
        ): Holiday {
            return Holiday(
                holidayType = HolidayType.Weekday,
                month = month,
                date = date,
                name = name
            )
        }

        fun recurringDay(
            name: String,
            month: Int,
            dayOfWeek: Int,
            weekOfMonth: Int,
        ): Holiday {
            return Holiday(
                holidayType = HolidayType.RecurringDay,
                name = name,
                month = month,
                dayOfWeek = dayOfWeek,
                weekOfMonth = weekOfMonth
            )
        }
    }

    fun key(): String = holidayType.generateKey(
        month = month,
        date = date,
        dayOfWeek = dayOfWeek,
        weekOfMonth = weekOfMonth
    )
}

enum class HolidayType {
    Fixed,
    Weekday,
    RecurringDay,
    None;

    fun generateKey(
        month: Int,
        @Nullable date: Int = -1,
        @Nullable dayOfWeek: Int = -1,
        @Nullable weekOfMonth: Int = -1
    ): String {
        return when (this) {
            None -> ""
            Fixed, Weekday -> "$this.m$month.d$date"
            RecurringDay -> "$this.m$month.wM$weekOfMonth.dW$dayOfWeek"
        }
    }
}

