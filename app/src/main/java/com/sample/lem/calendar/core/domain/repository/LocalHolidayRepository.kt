package com.sample.lem.calendar.core.domain.repository

import com.sample.lem.calendar.core.data.models.Holiday
import java.util.*
import javax.inject.Inject

class LocalHolidayRepository @Inject constructor() : HolidayRepository {
    override suspend fun getHolidays(): List<Holiday> {
        return arrayListOf(
            Holiday.fixed(
                name = "Holiday Fixed 1",
                month = Calendar.SEPTEMBER,
                date = 13,
            ),
            Holiday.fixed(
                name = "Holiday Fixed 2",
                month = Calendar.SEPTEMBER,
                date = 13,
            ),
            Holiday.weekday(
                name = "Holiday Weekday1",
                month = Calendar.SEPTEMBER,
                date = 19,
            ),
            Holiday.recurringDay(
                name = "Holiday Recurring",
                month = Calendar.SEPTEMBER,
                dayOfWeek = Calendar.TUESDAY,
                weekOfMonth = 2
            )
        )
    }
}