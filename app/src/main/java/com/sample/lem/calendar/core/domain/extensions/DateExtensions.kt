package com.sample.lem.calendar.core.domain.extensions

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.generateReadableDate() : String =
    SimpleDateFormat("MMM d yyyy").format(this)