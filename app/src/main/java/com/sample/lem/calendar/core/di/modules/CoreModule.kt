package com.sample.lem.calendar.core.di.modules

import com.sample.lem.calendar.core.domain.repository.HolidayRepository
import com.sample.lem.calendar.core.domain.repository.LocalHolidayRepository
import com.sample.lem.calendar.core.presentation.dispatchers.CoroutineDispatcher
import com.sample.lem.calendar.core.presentation.dispatchers.CoroutineDispatcherImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class CoreModule {

    @Provides
    fun getCoroutineDispatcher(coroutineDispatcherImpl: CoroutineDispatcherImpl): CoroutineDispatcher =
        coroutineDispatcherImpl

    @Provides
    fun getHolidayRepository(holidayRepository: LocalHolidayRepository): HolidayRepository =
        holidayRepository
}