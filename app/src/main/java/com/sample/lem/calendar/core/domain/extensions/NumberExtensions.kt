package com.sample.lem.calendar.core.domain.extensions

fun Int.isEven() = (this % 2) == 0