package com.sample.lem.calendar.core.presentation.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sample.lem.calendar.R
import com.sample.lem.calendar.core.data.models.Holiday

class HolidayRecyclerViewAdapter(
    private val holidayMap: Map<String, List<Holiday>>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val headerPositions = arrayListOf<Int>()
    private val holidaysOnRangeSize by lazy { holidayMap.values.flatten().size }
    private val holidaysOnRangeWithPlaceHolder by lazy { ArrayList(holidayMap.values.flatten()) }
    private val holidayHeaderTitles by lazy { holidayMap.keys.toList() }
    private var headerPointer = 0;

    companion object {
        const val HEADER = 0
        const val HOLIDAY = 1
    }

    init {
        var currentHeaderPosition = 0
        headerPositions.add(currentHeaderPosition)

        holidayMap.values.forEach { holidays ->
            holidaysOnRangeWithPlaceHolder.add(currentHeaderPosition, null)
            currentHeaderPosition+=holidays.size + 1
            headerPositions.add(currentHeaderPosition)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            headerPositions.contains(position) -> HEADER
            else -> HOLIDAY
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            HEADER -> R.layout.item_holiday_date_header
            else -> R.layout.item_holiday
        }.let{ layout ->
            LayoutInflater.from(parent.context).inflate(layout, parent, false)
        }.run {
            when(viewType){
                HEADER -> HolidayHeaderRecyclerViewHolder(this)
                else -> HolidayBodyRecyclerViewHolder(this)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HolidayHeaderRecyclerViewHolder -> {
                headerPointer = position
                holder.tvDateTitle.text = holidayHeaderTitles[headerPositions.indexOf(position)]
            }
            is HolidayBodyRecyclerViewHolder -> {
                holder.tvHolidayTitle.text = holidaysOnRangeWithPlaceHolder[position].name
            }
        }
    }

    override fun getItemCount(): Int = holidayHeaderTitles.size + holidaysOnRangeSize

    class HolidayHeaderRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDateTitle: TextView by lazy { itemView.findViewById(R.id.tvDateTitle) }
    }

    class HolidayBodyRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvHolidayTitle: TextView by lazy { itemView.findViewById(R.id.tvHolidayTitle) }
    }
}